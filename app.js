const http = require("http");
const https = require("https");
const fs = require("fs");
const express = require("express");
const debug = require('debug')('server-with-vhost:server');
const vhost = require("vhost");

// const some_server = require("../some/app");

const server = express();

// server.use(vhost("three.two.one", some_server));

server.use((req, res) => { res.status(404).send("Page Not Found!"); });

// server.use(express.static("public"));

const options = {
    key: fs.readFileSync("/etc/letsencrypt/live/osirp.ml/privkey.pem"),
    cert: fs.readFileSync("/etc/letsencrypt/live/osirp.ml/cert.pem"),
}

const http_server = http.createServer(server);
const https_server = https.createServer(options, server);

const http_port = 80;
const https_port = 443;

listen(http_server, http_port);
listen(https_server, https_port);

function listen(server, port) {
    server.listen(port, () => { debug(`HostManager listening on port ${port}`); });
}